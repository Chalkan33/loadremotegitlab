package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

const (
	echoEnv int = 0
)

func FormatEnvEcho(v ...interface{}) (interface{}, error) {
	echoEnv := v[echoEnv].(string)
	return echoEnv + " formated", nil
}

func GetDataFromAPI(v ...interface{}) (interface{}, error) {
	mockableResponse := make(map[string]string)
	resp, err := http.Get("https://demo2382123.mockable.io/somedata")
	if err != nil {
		log.Fatal(err)
		return "cannot fetch", nil
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
		return "cannot get body of response", nil
	}
	json.Unmarshal(body, &mockableResponse)
	return mockableResponse["data"], nil
}
