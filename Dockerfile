FROM golang:latest as plugin_builder
WORKDIR /app
COPY . .
WORKDIR /app/plugin/hello
RUN go mod download
RUN  GOOS=linux go build -buildmode=plugin -o hello.so
RUN mv hello.so /app

FROM chalkan33/managegitlabpipeline
WORKDIR /app
COPY . .
COPY --from=plugin_builder /app/hello.so plugins/

ENTRYPOINT ["/app/core"]